#include <SFML/Graphics.hpp>
#include "particle.h"

Particle::Particle(int x, int y, sf::Vector2f velocity)
{
	mX = x, mY = y;
	mVelocity = velocity;
	mCircle.setRadius(3);
}

Particle::Particle() {

}

void Particle::update() {
	if (mVelocity.x > 1.0) { mVelocity.x = 1.0; }
	if (mVelocity.x < 0.1) { mVelocity.x = 0.1; }
	if (mVelocity.y > 1.0) { mVelocity.y = 1.0; }
	mX += mVelocity.x, mY += mVelocity.y;
	int b = sqrt(mVelocity.x * mVelocity.x + mVelocity.y * mVelocity.y) * 150;
	if (b > 255) {
		b = 255;
	}
	mColor = sf::Color(25, 50, b);
	mCircle.setFillColor(mColor);
	mCircle.setPosition(mX, mY);
}

void Particle::setVelocity(sf::Vector2f velocity)
{
	mVelocity = velocity;
}

