#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include "particle.h"


float random_float(float min, float max) {
	float num = ((float)rand() / RAND_MAX) * (max - min) + min;
	return num;
}

sf::Vector2f normalize(sf::Vector2f vector) {
	float mag = sqrt(vector.x * vector.x + vector.y * vector.y);
	float x = vector.x * mag, y = vector.y * mag;
	return sf::Vector2f(x, y);
}

sf::Vector2f randomSpeed() {
	return sf::Vector2f(random_float(0.0, 1.0), random_float(-0.8, 0.8));
}

class River : public sf::Drawable, public sf::Transformable {
public:

	sf::VertexArray rLines1;
	sf::VertexArray rLines2;
	bool wasOut = false;

	void init(double lineCount, int windowWidth) {
		rLines1 = sf::VertexArray(sf::Points, lineCount);
		rLines2 = sf::VertexArray(sf::Points, lineCount);
		for (double ii = 0; ii < lineCount; ii++) {
			rLines1[ii].position = sf::Vector2f(ii, -150 * sin((ii / 100)) + 225);
			rLines2[ii].position = sf::Vector2f(ii, -150 * sin((ii / 100)) + 425);
			rLines1[ii].color = sf::Color::Magenta;
			rLines2[ii].color = sf::Color::Magenta;
		}
	}

	sf::Vector2f checkBounds(sf::Vector2f velocity, float x, float y) {
		if (y >= rLines2[(int)x].position.y) {
			float xAdd = 0.0;
			if (getSlope(x) < 0.0) {
				xAdd = -.01;
			}
			else {
				xAdd = -.01;
			}
			return sf::Vector2f(velocity.x - xAdd, velocity.y - 0.6);
		}
			
		if (y <= rLines1[(int)x].position.y) {
			float xAdd = 0.0;
			if (getSlope(x) > 0.0) {
				xAdd = 0.01;
			}
			else {
				xAdd = -.01;
			}
			return sf::Vector2f(velocity.x - xAdd, velocity.y + 0.6);
		}

		return velocity;
	}

	float getSlope(float x) {
		float slope = -6 / 2 * cos(x / 100);
		return slope;
		//return normalize(sf::Vector2f(1.0, slope));
	}

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const {
		// apply the entity's transform -- combine it with the one that was passed by the caller
		states.transform *= getTransform(); // getTransform() is defined by sf::Transformable

		target.draw(rLines1, states);
		target.draw(rLines2, states);
	}
};

int main()
{
	srand(time(NULL));

	sf::RenderWindow window(sf::VideoMode(1000, 600), "");
	std::vector<Particle> particles;
	River river;
	river.init(1000, window.getSize().x);

	for (int ii = 0; ii < 5000; ii++) {
		particles.push_back(Particle(0, random_float(225, 425), sf::Vector2f(randomSpeed())));
	}

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::White);

		for (int ii = 0; ii < particles.size(); ii++) {
			particles[ii].setVelocity(river.checkBounds(particles[ii].mVelocity, particles[ii].mX, particles[ii].mY));
			particles[ii].update();
			if (particles[ii].mX >= 999 or particles[ii].mX < 0) { particles[ii] = Particle(0, random_float(225, 425), sf::Vector2f(randomSpeed())); }
			window.draw(particles[ii]);
		}

		window.draw(river);

		window.display();
	}

	return 0;
}