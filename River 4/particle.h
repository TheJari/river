#include <SFML/Graphics.hpp>

class Particle : public sf::Drawable
{
public:
	Particle(int x, int y, sf::Vector2f velocity);
	Particle();

	float mX, mY;
	sf::Vector2f mVelocity;
	sf::CircleShape mCircle;
	sf::Color mColor;

	void update();
	void setVelocity(sf::Vector2f velocity);

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(mCircle, states);
	}
};
